import socket
import pickle

PORT = 5000
IP = "127.0.0.1"

tcp_cliente = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
tcp_cliente.connect((IP,PORT))
print("\033[31mBem-vindo ao CHAT DO IVANDRINHO 2000\033[m")
print("\033[31m--------- MENUUUUUUUUU ---------\033[m")
print("-- DIGITE 1 PARA ACESSAR O CHAT GLOBAL -- ")
print("-- DIGITE 2 PARA VISUALIZAR USUARIOS -- ")
print("-- DIGITE 3 PARA ENVIAR MENSAGEM NO PRIVADO -- ")
print("-- DIGITE 4 PARA ALTERAR O NICKNAME -- ")
print("-- DIGITE 5 PARA TRANSFERENCIA DE ARQUIVO DO SERVIDOR -- ")
print("-- DIGITE 0 PARA ATUALIZAR CHAT -- ")
print("\033[31m--------------------------------\033[m")
print("DIGITE exit PARA FINALIZAR A CONEXÃO NO CHAT DO IVANDRINHO 2000")
print("------------------------------------")
print("Digite seu nickname: ")
nick = input()
if nick == '':
    nick = "Jeremias"
mensagem = input("DIGITE sua mensagem de boas-vindas: ")
info = {
    "nick": nick,
    "msg": mensagem,
    "status": 'geral',
    "nick_aprovado": False,
    "sistema": None,
    "para_privado": None,
    "historico_msg_servidor": [],
    "msg_privado": [],
    "nome_doc": "",
    "conteudo_doc": None
}
envio = pickle.dumps(info)  # json
tcp_cliente.send(envio)
print("Mensagem enviada")
while mensagem != "exit":
    #tcp_cliente.send(nick.encode("ascii"))
    #tcp_cliente.send(mensagem.encode("ascii"))
    data = tcp_cliente.recv(1024)
    msg = pickle.loads(data)
    #print("\033[1;31;40mSERVIDOR\033[m")
    print(msg)

    if (msg['status'] == 'geral'):
        #print ("\n" * 130)
        if (len(msg['historico_msg_servidor']) > 0):
            for i in reversed(msg['historico_msg_servidor']):
                print("\033[1;32m"+i+"\033[m")

    if (msg['sistema'] != None):
        print("\033[1;31;40mSistema: {}\033[m".format(msg['sistema']))

    if(msg['nick_aprovado'] == False):
        print("\033[1;31;40mServidor: Nome já em uso...\033[m")
        novo_nick = input("Digite um novo nome: ")
        msg['nick'] = novo_nick

    if ( len(msg['msg_privado']) > 0):
        for i in reversed(msg['msg_privado']):
            if(i['destinatario'] == msg['nick']):
                print("\033[1;34mPrivado - {} : {}\033[m".format(i['remetente'], i['mensagem']))
                msg['status'] = 'geral'
                msg['para_privado'] = None
                msg['msg_privado'] = []


    if (len(msg['nome_doc'])> 0):
        file = open("tranferido",'wb')
        file_data = msg['conteudo_doc']
        #print(msg['conteudo_doc'])
        file.write(file_data)
        file.close()
        print("\033[33mTransferido\033[m")
        msg['conteudo_doc'] = None
        msg['nome_doc'] = ""

    mensagem = input("DIGITE: ")
    #info = {
    #    "nick": nick,
    #   "msg": mensagem,
    #    "status": 'geral',
    #    "nick_aprovado": msg['nick_aprovado'],
    #    "sistema": None
    # }
    msg['msg'] = mensagem

    if mensagem == '1':
        msg['status'] = "geral"
        msg['msg'] = info['nick'] + " Acessando o CHAT GLOBAL"
    elif mensagem == '2':
        msg['status'] = "visualizarUsuario"
        msg['msg'] = info['nick'] + " Visualizando os usuarios do CHAT"
    elif mensagem == '3':
        msg['status'] = "privado"
        msg['msg'] = info['nick'] + " Enviando Mensagem no PRIVADO"
        enviar_para = input("\033[1;33mDigite o nome da pessoa que deseja enviar mensagem no Privado:\033[m ")
        msg['para_privado'] = enviar_para
        mensagem_privada = input("\033[1;33mDigite a mensagem para {}:\033[m ".format(msg['para_privado']))
        msg['msg'] = mensagem_privada
    elif mensagem == '4':
        msg['status'] = "trocarNick"
        msg['msg'] = info['nick'] + " Trocando seu NICKNAME"
        trocar_nick = input("\033[1;36mDigite o novo nick:\033[m ")
        msg['nick'] = trocar_nick
        msg['nick_aprovado'] = False
    elif mensagem == '5':
        msg['conteudo_doc'] = None
        msg['nome_doc'] = ""
        msg['status'] = "transferencia"
        nome_documento = input(str("Nome do documento no servidor:"))
        msg['nome_doc'] = nome_documento
    elif mensagem == '0':
        msg['msg'] = "ATUALIZANDO O CHAT"

    envio = pickle.dumps(msg)  # json
    tcp_cliente.send(envio)

tcp_cliente.close()