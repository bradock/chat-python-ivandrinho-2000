import socket

HOST = "" # aceita qualquer ip
PORT = 5000
tcp = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
tcp.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1) # programa ficará na escuta
tcp.bind((HOST,PORT))
tcp.listen(10)
print("Aguardando Conexão")
con, endereco = tcp.accept()
print("CONECTADO: ",endereco)
while True:
    recebe = con.recv(1024).decode("ascii") # tamannho do buffer para receber a mensagem
    if not recebe:
        break
    print("Mensagem recebida: ", recebe)
tcp.close()
