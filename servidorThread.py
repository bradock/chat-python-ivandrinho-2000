import socket
import threading
import pickle

HOST = ""
PORT = 5000
USER = []
MSG_GERAL = []
MSG_PRIVADO = []
def conectado(con,endereco,USER,MSG_GERAL,MSG_PRIVADO):
    #print("Conectado por ",endereco)
    try:
        print("Cliente {} - CONECTADO".format(endereco))
        while True:
            #msg = con.recv(1024).decode("ascii")
            data = con.recv(1024)
            if not data:
                break
            msg = pickle.loads(data)
            #print(msg)

            if msg['nick'] in USER and msg['nick_aprovado'] == False:
                print("Já tem")
                msg['sistema'] = "NICK_POSSUI"
            else:
                if not(msg['nick'] in USER):
                     USER.insert(0, msg['nick'])
                msg['nick_aprovado'] = True

            #enviar lado do cliente


            if msg['status'] == 'geral':
                print("{} : {}".format(msg['nick'],msg['msg']))
                msg['sistema'] = None
                MSG_GERAL.insert(0,""+msg['nick']+" : "+msg['msg'])
                msg['historico_msg_servidor'] = MSG_GERAL
            elif msg['status'] == 'visualizarUsuario':
                msg['sistema'] = USER
                msg['status'] = 'geral'
            elif msg['status'] == 'privado':
                if(msg['para_privado'] in USER):
                    msg['status'] = msg['nick'] #remetente
                    msg['sistema'] = None
                    privado = {
                        "remetente": msg['nick'],
                        "destinatario": msg['para_privado'],
                        "mensagem": msg['msg']
                    }
                    MSG_PRIVADO.insert(0,privado)
                    msg['status'] = 'geral'
                else:
                    msg['sistema'] = "USUARIO_NÃO_ENCONTRADO"
                    msg['status']  = "geral"
            elif msg['status'] == 'trocarNick':
                if msg['nick'] in USER and msg['nick_aprovado'] == False:
                    print("Já tem")
                    msg['sistema'] = "NICK_POSSUI"
                    msg['status']  = "geral"
                else:
                    if not (msg['nick'] in USER):
                        USER.insert(0, msg['nick'])
                    msg['nick_aprovado'] = True
                    msg['status'] = "geral"
            elif msg['status'] == 'transferencia':
                file = open(msg['nome_doc'],'rb')
                file_data = file.read(1024)
                msg['conteudo_doc'] = file_data
                print("ARQUIVO ENVIADO")
                msg['status'] = "geral"

            msg['msg_privado'] = MSG_PRIVADO
            envio = pickle.dumps(msg)
            con.send(envio)
    except Exception:
        print("Erro no servidor")
    print("Finalizando a conexão do cliente ", endereco)
    con.close()
    #threading.exit()

tcp = socket.socket(socket.AF_INET,socket.SOCK_STREAM) # IP4 e TCP
tcp.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1) # programa ficará na escuta
tcp.bind((HOST,PORT))
tcp.listen(1)

print("-- CHAT IVANDRINHO 2000 --")
print("Esperando conexão")
while True:
    con, endereco = tcp.accept()
    t = threading.Thread(target=conectado, args= (con,endereco,USER,MSG_GERAL,MSG_PRIVADO))
    t.start()

tcp.close()

